# README #

### What is this repository for? ###

* I solved the first problem from here: http://pastebin.com/W9fRFRgm
> 1. Given a choice of geometric figures, calculate the chosen figure’s area and perimeter. The required lengths will also be input by the user. The user can choose between a circle, a triangle or a rectangle. User input should be taken via console.


### How do I get set up? ###

* This program was built with IntelliJ IDEA 15.0.4