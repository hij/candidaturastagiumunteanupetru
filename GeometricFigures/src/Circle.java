import java.util.IllegalFormatCodePointException;

/**
 * Created by M on 05.02.2017.
 */
public class Circle implements GeometricFigure {

    double radius;

    public Circle(){
        radius = 0;
    }

    public Circle(double r) throws IllegalArgumentException{
        if(r >= 0)
            radius = r;
        else throw new  IllegalArgumentException("Negative argument");
    }

    public void setRadius(double r){
        if(r >= 0)
            radius = r;
        else throw new  IllegalArgumentException("Negative argument");
    }

    public double getRadius(){
        return radius;
    }

    public double getArea(){
        return Math.PI * Math.pow(radius,2);
    }

    public double getPerimeter(){
        return 2 * Math.PI * radius;
    }

    public void printArea(){
        System.out.println("Circle area is " + getArea());
     }

    public void printPerimeter(){
        System.out.println("Circle circumference (perimeter) is " + getPerimeter());
    }

}
