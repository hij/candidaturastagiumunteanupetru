/**
 * Created by M on 05.02.2017.
 */
public class Rectangle implements GeometricFigure {

    double length, width;

    public Rectangle(){
        length = 0;
        width = 0;
    }

    public Rectangle(double l1, double l2) throws IllegalArgumentException{
        if(l1 < 0 || l2 < 0)
            throw new IllegalArgumentException("Negative arguments");

        if(l1 > l2) {
            length = l1;
            width = l2;
        }
        else {
            length = l2;
            width = l1;
        }
    }

    public void setSides(double l1, double l2) throws IllegalArgumentException{
        if(l1 < 0 || l2 < 0)
            throw new IllegalArgumentException("Negative arguments");

        if(l1 > l2) {
            length = l1;
            width = l2;
        }
        else {
            length = l2;
            width = l1;
        }
    }

    public void setLength(double length) {
        if (length >= this.width)
            this.length = length;
        else
            System.out.println("The value you introduced for length is smaller than the width. Nothing has changed. You must first change the width to a smaller value, or choose a larger value for length.");
    }

    public void setWidth(double width) {
        if(width <= this.length)
            this.width = width;
        else
            System.out.println("The value you introduced for width is larger than the length. Nothing has changed. You must first change the length to a larger value, or choose a smaller value for width.");
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public double getArea(){
        return length * width;
    }

    public double getPerimeter(){
        return 2 * (length + width);
    }

    public void printArea(){
        System.out.println("Rectangle area is " + getArea());
    }

    public void printPerimeter(){
        System.out.println("Rectangle perimeter is " + getPerimeter());
    }

}
