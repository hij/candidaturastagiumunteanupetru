import java.util.Scanner;

/**
 * Created by M on 05.02.2017.
 */
public class Main {

    public static void main(String[] args) {

        boolean ok;

        while (true) {
            // the menu
            System.out.println("\nChoose a figure: \n" +
                    "1 - circle \n" +
                    "2 - triangle \n" +
                    "3 - rectangle \n" +
                    "Any other key to quit \n");

            int option;

            Scanner scanIn = new Scanner(System.in);

            if (scanIn.hasNextInt()) {
                option = scanIn.nextInt();

                    switch (option) {
                        case 1:
                            System.out.println("\nIntroduce circle radius: ");
                            Circle circle = new Circle();

                            ok = false;
                            while (!ok) {
                                try {
                                    ok = true;
                                    circle.setRadius(input());
                                } catch (IllegalArgumentException e) {
                                    ok = false;
                                    System.out.println(e);
                                    System.out.println("Try again!");
                                }
                            }
                            circle.printPerimeter();
                            circle.printArea();
                            break;

                        case 2:
                            double a, b, c;
                            Triangle triangle = new Triangle();

                            ok = false;
                            while (!ok) {
                                System.out.println("\nIntroduce triangle sides: ");

                                System.out.print("a = ");
                                a = input();

                                System.out.print("b = ");
                                b = input();

                                System.out.print("c = ");
                                c = input();

                                try {
                                    ok = true;
                                    triangle.setSides(a, b, c);
                                } catch (IllegalArgumentException e) {
                                    ok = false;
                                    System.out.println(e);
                                    System.out.println("Try again!");
                                }
                            }
                            triangle.printArea();
                            triangle.printPerimeter();
                            break;

                        case 3:
                            double l1, l2;
                            Rectangle rectangle = new Rectangle();

                            ok = false;
                            while (!ok) {
                                System.out.println("\nIntroduce rectangle length and width");
                                System.out.print("Side one: ");
                                l1 = input();
                                System.out.print("Second side: ");
                                l2 = input();

                                try {
                                    ok = true;
                                    rectangle.setSides(l1, l2);
                                }catch (IllegalArgumentException e){
                                    ok = false;
                                    System.out.println(e);
                                    System.out.println("Try again!");
                                }
                            }
                            rectangle.printArea();
                            rectangle.printPerimeter();
                            break;

                        default:
                            scanIn.close();
                            System.out.println("Goodbye!");
                            System.exit(0);
                    }
            } else {
                scanIn.close();
                System.out.println("Goodbye!");
                System.exit(0);
            }

        }//end while()
    }


    public static double input(){
        Scanner scanI = new Scanner(System.in);
        boolean ok = false;
        double i = 0;

        do{
            if(scanI.hasNextDouble()){
                i = scanI.nextDouble();
                ok = true;}

            else {
                System.out.println("Input must be a number!");
                scanI.next();
            }
        }while (!ok);

        return i;
    }
}


