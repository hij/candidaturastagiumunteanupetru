/**
 * Created by M on 05.02.2017.
 */
public class Triangle implements GeometricFigure{

    double a, b, c;

    public Triangle(){
        a = 0;
        b = 0;
        c = 0;
    }

    public Triangle(double n1, double n2, double n3) throws IllegalArgumentException{
        if(n1 < 0 || n2 < 0 || n3 < 0)
            throw new IllegalArgumentException("Negative arguments");

        if((n1 + n2) >= n3 && (n1 + n3) >= n2 && (n2 + n3) >= n1){
            a = n1;
            b = n2;
            c = n3;
        }
        else throw new IllegalArgumentException("The values provided cannot form a triangle");
    }

    public void setSides(double n1, double n2, double n3) throws IllegalArgumentException{
        if(n1 < 0 || n2 < 0 || n3 < 0)
            throw new IllegalArgumentException("Negative arguments");

        if((n1 + n2) >= n3 && (n1 + n3) >= n2 && (n2 + n3) >= n1){
            a = n1;
            b = n2;
            c = n3;
        }
        else throw new IllegalArgumentException("The values provided cannot form a triangle");

    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() { return c; }

    public double getArea(){
        double p = getPerimeter() / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    public double getPerimeter(){
        return a + b + c;
    }

    public void printArea(){
        System.out.println("Triangle area is " + getArea());
    }

    public void printPerimeter(){
        System.out.println("Triangle perimeter is " + getPerimeter());
    }

}
