/**
 * Created by M on 05.02.2017.
 */
public interface GeometricFigure {

    // return the value
    public double getArea();
    public double getPerimeter();

    // print the value
    public void printArea();
    public void printPerimeter();
}
